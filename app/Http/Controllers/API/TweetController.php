<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTweetRequest;
use App\Http\Resources\TweetResource;
use App\Models\Tweet;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class TweetController extends Controller
{

    public function store(CreateTweetRequest $request): JsonResponse
    {
        $tweet = $request->user()->tweets()->create($request->validated());

        return response()->json([
            'tweet' => $tweet
        ]);
    }

    public function timeline(Request $request): AnonymousResourceCollection
    {
        $followingsIds = $request->user()->followings()->pluck('user_id');
        $tweets        = Tweet::whereIn('user_id', $followingsIds)->paginate();

        return TweetResource::collection($tweets);
    }
}
