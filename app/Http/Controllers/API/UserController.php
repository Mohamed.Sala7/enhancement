<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\FollowRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignUpRequest;
use App\Mail\WelcomeMail;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{

    public function signup(SignUpRequest $request): JsonResponse
    {
        $data             = $request->validated();
        $data['password'] = Hash::make($request->password);
        $user             = User::create($data);

        $token = $user->createToken('auth-token');

        Mail::to($user)->send(new WelcomeMail('welcome to our system'));

        return response()->json([
            'user'       => $user,
            'auth_token' => $token->plainTextToken
        ]);
    }


    public function login(LoginRequest $request): JsonResponse
    {
        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'message' => __('lang.invalid_credientials'),
            ]);
        }

        return response()->json([
            'auth_token' => $user->createToken('auth-token')->plainTextToken
        ]);
    }


    public function follow(FollowRequest $request): JsonResponse
    {
        $currentUser   = $request->user();
        $followingUser = User::find($request->user_id);

        $followedAlready = $currentUser->followings()->where('user_id', $request->user_id)->exists();
        if ($followedAlready) {
            return response()->json([
                'message' => __('lang.followed_already', ['name' => $followingUser->name])
            ], 404);
        }

        $currentUser->followings()->attach($followingUser);

        return response()->json([
            'message' => __('lang.followed_successfully', ['name' => $followingUser->name])
        ]);
    }
}
